package edu.bo.ucb

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Snackbar.make(layoutPrincipal , getString(R.string.mensaje_snackbar), Snackbar.LENGTH_LONG).show()

        printMessage.setText("Ninguno")

        btnServicios.setOnClickListener{
            printMessage.setText("Servicios")
        }

        btnPortafolio.setOnClickListener{
            printMessage.setText("Portafolio")
        }

        btnAcercaDe.setOnClickListener{
            printMessage.setText("Acerca de")
        }

        btnContacto.setOnClickListener{
            printMessage.setText("Contacto")
        }

        btnSocialMedia.setOnClickListener{
            printMessage.setText("Redes Sociales")
        }
    }

//    fun selectedButton(view: View){
//        System.out.println("entro aqui")
//        if(view.id === R.id.btnServicios){
//            Toast.makeText(view.context, "Opiones de Servicios", Toast.LENGTH_LONG).show()
//        } else {
//            Toast.makeText(view.context, "Evento no definido", Toast.LENGTH_LONG).show()
//        }
//    }

    fun selectedButton(view: View) {
        if ( view.id === R.id.btnServicios) {
            val intent: Intent = Intent(view.context, ServiceActivity::class.java)
            startActivity(intent)

        } else {
            Toast.makeText( view.context, "Evento no definido", Toast.LENGTH_LONG).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {

            R.id.action_search ->{
                Log.d("TEST", "ICON SEARCH")
                true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
